all: src/fflib.c
	gcc -Wall -O -fPIC -shared -o bin/libff.so src/fflib.c -lavutil -lavfilter -lavformat -lavcodec -lswscale -lz -lm -lswresample  `sdl-config --cflags --libs`
install:
	cp bin/libff.so /usr/lib
clear:
	rm bin/libff.so
